### How to run


To run containers
```
> mvn clean install
> docker-compose up -d
```

To stop containers
```
> docker-compose down
```


### REST API


Get balance

**id** - account id
```
GET http://localhost:8080/account/{id}/balance
```
 
Get all transfers of account

**id** - account id
```
GET http://localhost:8080/account/{id}/transfers
```

Make transfer
```
POST http://localhost:8080/transfer
Content-Type: application/json
```

Body:

**fromId** - sender id 

**toId** - recipient id

**amount** - amount (F.E. 10 or 10.22 or 10.2)
```json
{
  "from": {fromId},
  "to": {toId},
  "amount": {amount}
}
```

