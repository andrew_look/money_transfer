package com.open.tool.money_transfer.service;

import com.open.tool.money_transfer.exception.BaseException;
import com.open.tool.money_transfer.model.db.Account;
import com.open.tool.money_transfer.repository.AccountRepository;
import com.open.tool.money_transfer.repository.TransferRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TransferServiceTest {

    private AccountRepository accountRepository;
    private TransferRepository transferRepository;
    private TransferService transferService;
    private AccountService accountService;
    private Account firstAccount;
    private Account secondAccount;
    private final Long first = 1L;
    private final Long second = 2L;

    @BeforeEach
    void setUp() {
        this.accountRepository = mock(AccountRepository.class);
        this.transferRepository = mock(TransferRepository.class);
        this.transferService = new TransferService(accountRepository, transferRepository);
        this.accountService = new AccountService(accountRepository);

        firstAccount = Account.builder()
                .balance(BigDecimal.valueOf(100L))
                .build();
        secondAccount = Account.builder()
                .balance(BigDecimal.valueOf(200L))
                .build();
        when(accountRepository.findById(first)).thenReturn(Optional.of(firstAccount));
        when(accountRepository.findById(second)).thenReturn(Optional.of(secondAccount));
        when(accountRepository.existsById(first)).thenReturn(true);
        when(accountRepository.existsById(second)).thenReturn(true);
        when(accountRepository.getOne(first)).thenReturn(firstAccount);
        when(accountRepository.getOne(second)).thenReturn(secondAccount);
    }

    @Test
    void makeTransfer() {

        BigDecimal amount = BigDecimal.valueOf(2.5);
        transferService.makeTransfer(first, second, amount);

        assertEquals(BigDecimal.valueOf(100L).subtract(amount), accountService.getBalance(first).getBalance());
        assertEquals(BigDecimal.valueOf(200L).add(amount), accountService.getBalance(second).getBalance());
    }


    @Test
    void makeTransferNegativeAmount() {
        BigDecimal amount = BigDecimal.valueOf(-2.5);
        Assertions.assertThrows(BaseException.class, () -> transferService.makeTransfer(first, second, amount));
    }
}