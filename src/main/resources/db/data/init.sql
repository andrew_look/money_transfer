# DDL

create table account
(
    id      bigint auto_increment primary key,
    balance decimal(19, 2) null
);

create table transfer
(
    id           varchar(255)   not null primary key,
    amount       decimal(19, 2) null,
    created_dttm datetime       null,
    from_id      bigint         null,
    to_id        bigint         null,
    constraint account_from_id_fk
        foreign key (from_id) references account (id),
    constraint account_to_id_fk
        foreign key (to_id) references account (id)
);


# DATA


INSERT INTO account (id, balance)
VALUES (1, 100.00);
INSERT INTO account (id, balance)
VALUES (2, 200.00);
INSERT INTO account (id, balance)
VALUES (3, 300.00);
