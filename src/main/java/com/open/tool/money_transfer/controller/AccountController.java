package com.open.tool.money_transfer.controller;

import com.open.tool.money_transfer.exception.BaseException;
import com.open.tool.money_transfer.model.resp.BalanceResponse;
import com.open.tool.money_transfer.model.resp.GenericResponse;
import com.open.tool.money_transfer.model.resp.TransferResponse;
import com.open.tool.money_transfer.service.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/account/{id}")
public class AccountController {

    private final AccountService accountService;

    @GetMapping("/balance")
    private GenericResponse<BalanceResponse> getBalance(@PathVariable("id") Long accountId) {
        try {
            return new GenericResponse<>(accountService.getBalance(accountId), HttpStatus.OK);
        } catch (BaseException ex) {
            return new GenericResponse<>(ex.getStatus(), ex.getMessage());
        }
    }

    @GetMapping("/transfers")
    private GenericResponse<List<TransferResponse>> getTransfers(@PathVariable("id") Long accountId) {
        try {
            return new GenericResponse<>(accountService.getTransfers(accountId), HttpStatus.OK);
        } catch (BaseException ex) {
            return new GenericResponse<>(ex.getStatus(), ex.getMessage());
        }
    }

}
