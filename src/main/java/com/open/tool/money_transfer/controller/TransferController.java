package com.open.tool.money_transfer.controller;


import com.open.tool.money_transfer.exception.BaseException;
import com.open.tool.money_transfer.model.req.TransferRequest;
import com.open.tool.money_transfer.model.resp.GenericResponse;
import com.open.tool.money_transfer.service.AccountService;
import com.open.tool.money_transfer.service.TransferService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
public class TransferController {

    private final TransferService transferService;
    private final AccountService accountService;

    @PostMapping("/transfer")
    private GenericResponse<Void> makeTransfer(@RequestBody TransferRequest req) {

        if (req.getTo() == null) {
            return new GenericResponse<>(HttpStatus.BAD_REQUEST, "Recipient should be presented");
        }

        if (req.getFrom() == null) {
            return new GenericResponse<>(HttpStatus.BAD_REQUEST, "Sender should be presented");
        }
        if (req.getAmount() == null) {
            return new GenericResponse<>(HttpStatus.BAD_REQUEST, "Amount should be presented");
        }

        try {
            transferService.makeTransfer(req.getFrom(), req.getTo(), req.getAmount());
        } catch (BaseException ex) {
            return new GenericResponse<>(HttpStatus.BAD_REQUEST, ex.getMessage());
        }


        return new GenericResponse<>(HttpStatus.OK);
    }
}
