package com.open.tool.money_transfer.service;


import com.open.tool.money_transfer.exception.ObjectNotFound;
import com.open.tool.money_transfer.model.db.Account;
import com.open.tool.money_transfer.model.resp.BalanceResponse;
import com.open.tool.money_transfer.model.resp.TransferResponse;
import com.open.tool.money_transfer.repository.AccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

@AllArgsConstructor
@Service
public class AccountService {


    private final AccountRepository accountRepository;

    public BalanceResponse getBalance(Long accountId) {
        if (!accountRepository.existsById(accountId)) {
            throw new ObjectNotFound("Account by id: " + accountId + " not found", HttpStatus.NOT_FOUND);
        }

        return new BalanceResponse(accountRepository.getOne(accountId).getBalance());
    }


    public List<TransferResponse> getTransfers(Long accountId) throws ObjectNotFound {

        Account from = accountRepository.findById(accountId)
                .orElseThrow(() -> new ObjectNotFound("Account by id: " + accountId + " not found", HttpStatus.NOT_FOUND));

        List<TransferResponse> transfers = new ArrayList<>();


        Stream.of(from.getTransfersFrom(), from.getTransfersTo())
                .flatMap(Collection::stream)
                .map(element -> TransferResponse.builder()
                        .amount(element.getAmount())
                        .toId(element.getTo().getId())
                        .fromId(element.getFrom().getId())
                        .createDate(element.getCreatedDttm())
                        .build())
                .forEach(transfers::add);

        transfers.sort(Comparator.comparing(TransferResponse::getCreateDate).reversed());

        return transfers;
    }
}
