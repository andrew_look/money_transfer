package com.open.tool.money_transfer.service;


import com.open.tool.money_transfer.exception.BaseException;
import com.open.tool.money_transfer.exception.InsufficientFundsException;
import com.open.tool.money_transfer.exception.ObjectNotFound;
import com.open.tool.money_transfer.model.db.Account;
import com.open.tool.money_transfer.model.db.Transfer;
import com.open.tool.money_transfer.repository.AccountRepository;
import com.open.tool.money_transfer.repository.TransferRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;

@AllArgsConstructor
@Service
public class TransferService {

    private final AccountRepository accountRepository;
    private final TransferRepository transferRepository;


    public synchronized void makeTransfer(Long fromId, Long toId, BigDecimal amount) throws ObjectNotFound {
        if (amount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new BaseException("Amount can't be negative", HttpStatus.BAD_REQUEST);
        }

        if(fromId.equals(toId)) {
            throw new BaseException("Sender and Recipient can't be equals", HttpStatus.BAD_REQUEST);
        }

        Account from = accountRepository.findById(fromId)
                .orElseThrow(() -> new ObjectNotFound("Account by id: " + fromId + " not found", HttpStatus.NOT_FOUND));
        Account to = accountRepository.findById(toId)
                .orElseThrow(() -> new ObjectNotFound("Account by id: " + toId + " not found", HttpStatus.NOT_FOUND));

        if (from.getBalance().compareTo(amount) < 0) {
            throw new InsufficientFundsException(String.format("Account(id:%d) insufficient founds", fromId), HttpStatus.BAD_REQUEST);
        }

        from.setBalance(from.getBalance().subtract(amount));
        to.setBalance(to.getBalance().add(amount));

        accountRepository.saveAll(Arrays.asList(from, to));
        transferRepository.save(Transfer.builder()
                .from(from)
                .to(to)
                .amount(amount)
                .build());
    }
}
