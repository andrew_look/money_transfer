package com.open.tool.money_transfer.repository;

import com.open.tool.money_transfer.model.db.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;


@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {


    @Query("select acc.balance from Account acc where acc.id = :id")
    BigDecimal getBalanceById(@Param("id") Long id);

}
