package com.open.tool.money_transfer.repository;

import com.open.tool.money_transfer.model.db.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TransferRepository extends JpaRepository<Transfer, String> {


}
