package com.open.tool.money_transfer.exception;


import org.springframework.http.HttpStatus;

public class InsufficientFundsException extends BaseException {

    public InsufficientFundsException(HttpStatus status) {
        super(status);
    }

    public InsufficientFundsException(String s, HttpStatus status) {
        super(s, status);
    }

    public InsufficientFundsException(String s, Throwable throwable, HttpStatus status) {
        super(s, throwable, status);
    }
}
