package com.open.tool.money_transfer.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;


@Getter
public class BaseException extends RuntimeException {

    private HttpStatus status;

    public BaseException(HttpStatus status) {
        this.status = status;
    }

    public BaseException(String s, HttpStatus status) {
        super(s);
        this.status = status;
    }

    public BaseException(String s, Throwable throwable, HttpStatus status) {
        super(s, throwable);
        this.status = status;
    }
}
