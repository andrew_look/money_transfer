package com.open.tool.money_transfer.exception;


import org.springframework.http.HttpStatus;

public class ObjectNotFound extends BaseException {

    public ObjectNotFound(HttpStatus status) {
        super(status);
    }

    public ObjectNotFound(String s, HttpStatus status) {
        super(s, status);
    }

    public ObjectNotFound(String s, Throwable throwable, HttpStatus status) {
        super(s, throwable, status);
    }
}
