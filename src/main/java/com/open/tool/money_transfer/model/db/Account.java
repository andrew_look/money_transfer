package com.open.tool.money_transfer.model.db;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private BigDecimal balance;

    @OneToMany(mappedBy = "to", cascade = CascadeType.REMOVE)
    private List<Transfer> transfersTo;

    @OneToMany(mappedBy = "from", cascade = CascadeType.REMOVE)
    private List<Transfer> transfersFrom;

}
