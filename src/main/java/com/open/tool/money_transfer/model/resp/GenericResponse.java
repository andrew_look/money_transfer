package com.open.tool.money_transfer.model.resp;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class GenericResponse<T> extends ResponseEntity<GenericResponse.ResponseEnvelope<T>> {

    public GenericResponse(HttpStatus status) {
        super(ResponseEnvelope.success(null), status);
    }

    public GenericResponse(T response, HttpStatus status) {
        super(ResponseEnvelope.success(response), status);
    }

    public GenericResponse(HttpStatus status, String errorMessage) {
        super(ResponseEnvelope.error(errorMessage), status);
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ResponseEnvelope<T> {
        private String errorMessage;
        private T response;

        public static <T> ResponseEnvelope<T> error(String errorMessage) {
            return ResponseEnvelope.<T>builder().errorMessage(errorMessage).build();
        }

        public static <T> ResponseEnvelope<T> success(T response) {
            return ResponseEnvelope.<T>builder().response(response).build();
        }
    }
}
