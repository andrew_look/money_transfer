package com.open.tool.money_transfer.model.resp;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransferResponse {
    private Long toId;
    private Long fromId;
    private BigDecimal amount;
    private LocalDateTime createDate;
}
