package com.open.tool.money_transfer.model.resp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


@AllArgsConstructor
@Data
@Builder
@NoArgsConstructor
public class BalanceResponse {
    private BigDecimal balance;
}
