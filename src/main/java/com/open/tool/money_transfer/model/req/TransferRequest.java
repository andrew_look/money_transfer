package com.open.tool.money_transfer.model.req;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class TransferRequest {
    private Long from;
    private Long to;
    private BigDecimal amount;
}
