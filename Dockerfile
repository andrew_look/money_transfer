FROM openjdk:8-jre
MAINTAINER Andrei Titov <amigodebian@gmail.com>
WORKDIR .

ADD target/money_transfer-1.0.0-SNAPSHOT.jar /base/money_transfer.jar

ENTRYPOINT ["java", "-jar","/base/money_transfer.jar"]